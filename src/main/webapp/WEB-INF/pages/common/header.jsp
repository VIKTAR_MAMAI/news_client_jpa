<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/style.css' />">
<table id="header" style="border-bottom: 3px solid black;">
	<tr>
		<td id="cell1"><a href="<c:url value="/news/page/1" />"><spring:message
					code="label.news.portal" /></a></td>
		<td id="cell2">
			<table id="right-table">
				<tr>
					<td><a href="?lang=en"><spring:message
								code="label.lang.english" /></a> | <a href="?lang=ru"><spring:message
								code="label.lang.russian" /></a></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
